# Над проектом працювали Ольга Хомич та Тищенко Сергій.

Проект складається з двох частин front end та back end.

## front end:

Реалізован на бібліотеці [React](https://reactjs.org/)

доп пакети:

- [ ] [redux](https://redux.js.org/) менеджер управління станом
- [ ] [react-redux](https://react-redux.js.org/) прив'язка інтерфейсу користувача React для Redux.
- [ ] [react-infinite-scroll-component](https://www.npmjs.com/package/react-infinite-scroll-component) нескінченне прокручування.

## back end:

- [ ] [ejs](https://www.npmjs.com/package/ejs) шаблонизатор 
- [ ] [express](https://expressjs.com/) веб-фреймворк для Node.js 
- [ ] [jsonwebtoken](https://www.npmjs.com/package/jsonwebtoken) шифрування дешефрування токена 
- [ ] [mongoose](https://mongoosejs.com/) бібліотека JS для роботи з БД MongoDB
- [ ] [bcryptjs](https://www.npmjs.com/package/bcryptjs) хешування пароля перед надсиланням в БД
- [ ] [cors](https://www.npmjs.com/package/cors) (Спільне використання ресурсів між джерелами) для створення додаткових HTTP-заголовків
- [ ] [dotenv](https://www.npmjs.com/package/dotenv) змінні середовища
- [ ] [express-validator](https://express-validator.github.io/docs) для валідації пароля та логіна при реєстрації (реалізовано на рівні api не інтегровано)
- [ ] [method-override](https://www.npmjs.com/package/method-override) дозволяє використовувати дієслова HTTP де клієнт їх не підтримує
- [ ] [morgan](https://www.npmjs.com/package/morgan) для логування HTTP-запитів

## Додаткові пакети для розробки:

- [ ] [nodemon](https://www.npmjs.com/package/nodemon) автоматично перезапускає сервер
- [ ] [Метамова SASS](https://sass-lang.com/) для зручності роботи з CSS у синтаксисі SCSS

## Додатково додані скрипти:

- [ ] для збірки графічного інтерфейсу покриття тестів
- [ ] для запуску та підтримки сервера при розробці

```
"coverage": "react-scripts test --coverage" 
"dev": "nodemon server/index.js" 
```

## Ольга Хомич працювала над:

- [ ] створення структури реакт-додатка
- [ ] створення структури об'єктів, які зберігаються в БД
- [ ] наповнення контента
- [ ] створення компонентів та сторінок додатку
- [ ] логіка лайків
- [ ] логіка підписок
- [ ] створення асинхронних функцій запитів на сервер
- [ ] створення редьюсеров
- [ ] реалізація логіки infinite-scroll

## Тищенко Сергій працював над:

- [ ] створення структури сервера
- [ ] підключення до БД
- [ ] розробка графічного інтерфейсу для ручного управління даними в БД
- [ ] розробка API інтерфейсу для взаємодії з додатком
- [ ] розробка окремої сторінки в реакті для тестування асинхронних функцій ExtraReducersTest
- [ ] настройка сховища станов react-redux redux redux-persist
- [ ] створення в сховищі обертки тестування асинхронних функцій
- [ ] розробка тестів
- [ ] вибраному компоненту для тестування Aside - зроблен рефакторинг
- [ ] для 3 асинхронних функцій написані тести для вдалих і не вдалих запитів

## Гайд по інтерфейсу сервера:

#### API

- повертає всі об'єкти в JSON-не
`/api/comments`
`/api/posts`
`/api/users`
- повертає один об'єкт по id в JSON-не
`/api/comments/:id`
`/api/post/:id`
`/api/user/:id`
- видаляє об'єкт
`/api/comments/delete/:id`
`/api/posts/delete/:id`
`/api/users/delete/:id`
- постить на сервер
 `/api/users/add`
`/api/posts/add`
`/api/users/add`
- обробка put-запитів редагування
`/api/comments/edit/:id`
`/api/posts/edit/:id`
`/api/users/edit/:id`

#### UI

- теж саме для графічного інтерфейсу але без `/api`
- крім видалення, наприклад: `/posts/delete` повертає список з можливістю видалення елемента вибірково

