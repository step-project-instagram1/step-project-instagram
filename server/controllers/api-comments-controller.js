import Comments from '../models/comments.js';

export const getJsonAll =(req,res)=>{
    Comments
        .find()
        .then(item => res.status(200).json(item))
        .catch(error => res.status(500).send(error))
}

export const getJson =(req,res)=>{
    Comments
        .findById(req.params.id)
        .then(item => res.status(200).json(item))
        .catch(error => res.status(500).send(error))
}

export const deleteObj =(req,res)=>{
    Comments
        .findByIdAndDelete(req.params.id)
        .then(_ => res.sendStatus(200))
        .catch(error => res.status(500).send(error))
}

export const addObj =(req,res)=>{
    const {userId, text, postId} = req.body
    const comments = new Comments({
        userId,
        postId,
        text,
        time: new Date().toLocaleString(),
    })
    comments
        .save()
        .then(user=> res.status(200).json(user))
        .catch(error => res.status(500).send(error))
}

export const editObj =(req,res)=>{
    const {userId, text, postId} = req.body, {id} = req.params;
    Comments
        .findByIdAndUpdate(id, {userId, text, postId}, {new:true})
        .then(user => res.status(200).json(user))
        .catch(error => res.status(500).send(error))
}