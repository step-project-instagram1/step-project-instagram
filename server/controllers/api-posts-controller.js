import Posts from '../models/posts.js';

export const getJsonAll =(req,res)=>{
    Posts
        .find()
        .then(item => res.status(200).json(item))
        .catch(error => res.status(500).send(error))
}

export const getJson =(req,res)=>{
    Posts
        .findById(req.params.id)
        .then(item => res.status(200).json(item))
        .catch(error => res.status(500).send(error))
}

export const deleteObj =(req,res)=>{
    Posts
        .findByIdAndDelete(req.params.id)
        .then(_ => res.sendStatus(200))
        .catch(error => res.status(500).send(error))
}

export const addObj =(req,res)=>{
    const {userId, text, photo, likes} = req.body
    const posts = new Posts({
        userId,
        text,
        photo,
        likes: JSON.parse(likes),
        time: new Date().toLocaleString(),
    })
    posts
        .save()
        .then(user=> res.status(200).json(user))
        .catch(error => res.status(500).send(error))
}

export const editObj =(req,res)=>{
    let {userId, text, photo, likes} = req.body, {id} = req.params;
        likes = JSON.parse(likes);
    Posts
        .findByIdAndUpdate(id, {userId, text, photo, likes}, {new:true})
        .then(user => res.status(200).json(user))
        .catch(error => res.status(500).send(error))
}