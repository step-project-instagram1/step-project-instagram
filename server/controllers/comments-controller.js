import Comments from '../models/comments.js';
import createPath from '../helpers/create-path.js';
const title = "comments";
const pattern = ['userId', 'text', 'postId'];

export const deletePage =(req,res)=>{
    Comments
        .find()
        .then(items => res.render(createPath('delete'), {items, title}))
        .catch(error => res.send(error))
}

export const deleteObj =(req,res)=>{
    Comments
        .findByIdAndDelete(req.params.id)
        .then(_ => res.sendStatus(200))
        .catch(error => res.send(error))
}

export const addPage =(req,res)=>{
    res.render(createPath('add'), {title, pattern})
}

export const addObj =(req,res)=>{
    const {userId, text, postId} = req.body
    const comments = new Comments({
        userId,
        postId,
        text,
        time: new Date().toLocaleString(),
    })
    comments
        .save()
        .then((reg) => {
            const res_serv = reg._doc;
            res.render(createPath('response'), {res_serv, title});
        })
        .catch(error => res.send(error))
}

export const editPage =(req,res)=>{
    Comments
        .findById(req.params.id)
        .then((reg) => {
            const item = reg._doc;
            res.render(createPath('edit'), {item, title});
        })
        .catch(error => res.send(error))
}

export const editObj =(req,res)=>{
    const {userId, text, postId} = req.body, {id} = req.params;
    Comments
        .findByIdAndUpdate(id, {userId, text, postId})
        .then(() => res.redirect(`/${title}/edit/${id}`))
        .catch(error => res.status(500).send(error))
}