import Posts from '../models/posts.js';
import createPath from '../helpers/create-path.js';
const title = "posts";
const pattern = ['userId', 'text', 'photo', 'likes'];

export const deletePage =(req,res)=>{
    Posts
        .find()
        .then(items => res.render(createPath('delete'), {items, title}))
        .catch(error => res.send(error))
}

export const deleteObj =(req,res)=>{
    Posts
        .findByIdAndDelete(req.params.id)
        .then(_ => res.sendStatus(200))
        .catch(error => res.send(error))
}

export const addPage =(req,res)=>{
    res.render(createPath('add'), {title, pattern})
}

export const addObj =(req,res)=>{
    const {userId, text, photo, likes} = req.body
    const posts = new Posts({
        userId,
        text,
        photo,
        likes: JSON.parse(likes),
        time: new Date().toLocaleString(),
    })
    posts
        .save()
        .then((reg) => {
            const res_serv = reg._doc;
            res.render(createPath('response'), {res_serv, title});
        })
        .catch(error => res.send(error))
}

export const editPage =(req,res)=>{
    Posts
        .findById(req.params.id)
        .then((reg) => {
            const item = reg._doc;
            console.log(item)
            res.render(createPath('edit'), {item, title});
        })
        .catch(error => res.send(error))
}

export const editObj =(req,res)=>{
    let {userId, text, photo, likes} = req.body, {id} = req.params;
    likes = JSON.parse(likes);
    Posts
        .findByIdAndUpdate(id, {userId, text, photo, likes})
        .then(() => res.redirect(`/${title}/edit/${id}`))
        .catch(error => res.status(500).send(error))
}