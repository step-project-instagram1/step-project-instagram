import {Schema, model} from 'mongoose';

const roleSchema = new Schema({
    value: {
        type: String,
        unique: true,
        default: "USER",
    },
}, {timestamps: true});

export default model("auth-roles", roleSchema);