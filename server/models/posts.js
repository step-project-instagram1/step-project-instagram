import mongoose from 'mongoose';

const postsSchema = new mongoose.Schema({
    userId: {
        type: String,
        required: true,
    },
    text: {
        type: String,
        required: true,
    },
    photo: {
        type: String,
        required: true,
    },
    likes: {
        type: Array,
        required: true,
    },
    time: {
        type: String,
    },
},{timestamps: true});

const Posts = mongoose.model("posts", postsSchema);

export default Posts