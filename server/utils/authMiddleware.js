import jwt from 'jsonwebtoken';
import secure from "../secure/config.js"
import createPath from "../helpers/create-path.js";

const title = "Авторизуйтесь !", subject = "";
const pattern = ['username', 'password'];

export default function authMiddleware(req, res, next) {
    if (req.method === "OPTIONS") {
        next()
    }
    try {
        if (req.body.password && req.body.username && req.method === "POST") {
            console.log("переадресация")
            res.redirect("/auth/login")
        }

        //const token = req.headers.authorization?.split(' ')[1] || null;
        console.log(req.rawHeaders)
        const token = req.rawHeaders.find(_=>_.includes("token"))?.split(';').find(_=>_.includes("token"))?.split('=')[1] || null;

        if (!token) {
            return res
                .render(createPath('add'), {title, pattern, subject, action: "/auth/login?_method=POST"})
        }

        req.user = jwt.verify(token, secure.secret)
        console.log(req.user)
        next()
    } catch (err) {
        console.log(err)
        if(err.toString().includes('Token')){
            return  res.render(createPath('add'), { title, pattern, subject: `Помилка токена ${err}`, action: "/auth/login?_method=POST"})
        }
        return  res.render(createPath('answer'), {massage: `Помилка ${err}`})
    }
}
