import './styles/style.scss';
import {Route, Routes, useParams} from "react-router-dom";
import Layout from "./pages/Layout";
import Main from "./pages/Main";
import Profile from "./pages/Profile";
import ExtraReducersTest from "./pages/ExtraReducersTest";
import {useDispatch, useSelector} from "react-redux";
import {useEffect} from "react";
import {setFriends, setIsFriend, setLikes, setUserId} from "./storage/slice";
import {
    editPost,
    editUser, loadPosts
} from "./storage/asyncThunks"

function App() {
    const dispatch = useDispatch()
    const {account, like, isFriend} = useSelector(state => ({
        account: state.my.activeAccount,
        like: state.my.like,
        isFriend:state.my.isFriend,
    }))

    function addFriend(e, id){
        let usersId
        typeof id === 'object' ? usersId = id.id : usersId = id
        let list
        if (isFriend.find(el => el === usersId)) {
            dispatch(setIsFriend(isFriend.filter(e => e !== usersId)))
            list = isFriend.filter(e => e !== usersId)
        } else {
            dispatch(setIsFriend([...isFriend, usersId]));
            list = [...isFriend, usersId]
        }
        let params = {
            id: account.id,
            photo: account.photo,
            friends: JSON.stringify(list),
            fullName:account.fullName,
            nickName:account.nickName
        }
        dispatch(editUser(params))
    }
    function likePhoto(e, id, likes, userId, text, postImg){
        let list
        if(like.includes(id.postId)){
            dispatch(setLikes(like.filter(e=> e !== id.postId)))
            list = likes.likes.filter(e=> e !== account.id)
        } else{
            dispatch(setLikes([...like, id.postId]))
            list = [...likes.likes, account.id]
        }
        let params = {
            id: id.postId,
            userId: userId.userId,
            text: text.text,
            photo: postImg.postImg,
            likes: JSON.stringify(list)
        }
        dispatch(editPost(params))
    }
    return (
        <Routes>
            <Route path='/' element={<Layout/>}>
                <Route index element={<Main
                    addFriend={addFriend}
                    likePhoto={likePhoto}
                />}/>
                <Route path='/profile/:id' element={<Profile
                    addFriend={addFriend}
                    likePhoto={likePhoto}
                />}/>
                <Route path='/extra-reducers-test' element={<ExtraReducersTest/>}/>
            </Route>
        </Routes>
    )
}

export default App;
