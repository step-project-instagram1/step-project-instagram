import {screen} from '@testing-library/react';
import '@testing-library/jest-dom'
import Aside from "../../components/Aside";
import {MemoryRouter} from "react-router-dom";
import {renderWithProviders} from '../../storage/test-utils';

describe('tests Aside', () => {

    it('snapshot renders', () => {
        const component = renderWithProviders(<MemoryRouter><Aside/></MemoryRouter>)
        expect(component).toMatchSnapshot();
    });

    it('renders search item', async () => {
        renderWithProviders(<MemoryRouter><Aside/></MemoryRouter>)
        expect(await screen.findByText(/Головна/i)).toBeInTheDocument()
        expect(await screen.findByText(/Цікаве/i)).toBeInTheDocument()
        expect(await screen.findByText(/Reels/i)).toBeInTheDocument()
        expect(await screen.findByText(/Повідомлення/i)).toBeInTheDocument()
        expect(await screen.findByText(/Сповіщення/i)).toBeInTheDocument()
        expect(await screen.findByText(/Профіль/i)).toBeInTheDocument()
    });

    it('renders list', async () => {
        renderWithProviders(<MemoryRouter><Aside/></MemoryRouter>)
        expect(await screen.getByRole('list')).toBeInTheDocument()
    });
})