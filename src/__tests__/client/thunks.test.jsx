import {loadComments, loadPosts, loadUsers,} from "../../storage/asyncThunks"

global.fetch = jest.fn();

describe('tests asyncThunks', () => {

    describe('ok: true', () => {

        const mockComments = [{userId: 123, postId: 456, text: 'text', time: '18:00'}]

        it('loadComments', async () => {
            const dispatch = jest.fn(),
                  thunk = loadComments();

            fetch.mockResolvedValue({
                ok: true,
                json: () => Promise.resolve(mockComments)
            })

            await thunk(dispatch, () => ({}));
            const {calls} = dispatch.mock;
            const [start, end] = calls;

            expect(calls).toHaveLength(2);
            expect(start[0].type).toBe(loadComments.pending().type);
            expect(end[0].type).toBe('my/comments/fulfilled');
            expect(end[0].payload).toBe(mockComments);
        });

        it('loadPosts', async () => {
            const dispatch = jest.fn(),
                  thunk = loadPosts();

            fetch.mockResolvedValue({
                ok: true,
                json: () => Promise.resolve(mockComments)
            })

            await thunk(dispatch, () => ({}));
            const {calls} = dispatch.mock;
            const [start, end] = calls;

            expect(calls).toHaveLength(2);
            expect(start[0].type).toBe('my/posts/pending');
            expect(end[0].type).toBe('my/posts/fulfilled');
            expect(end[0].payload).toBe(mockComments);
        });

        it('loadUsers', async () => {
            const dispatch = jest.fn(),
                  thunk = loadUsers();

            fetch.mockResolvedValue({
                ok: true,
                json: () => Promise.resolve(mockComments)
            })

            await thunk(dispatch, () => ({}));
            const {calls} = dispatch.mock;
            const [start, end] = calls;

            expect(calls).toHaveLength(2);
            expect(start[0].type).toBe(loadUsers.pending().type);
            expect(end[0].type).toBe(loadUsers.fulfilled().type);
            expect(end[0].payload).toBe(mockComments);
        });
    })
    describe('ok: false', () => {

        it('loadComments', async () => {
            const dispatch = jest.fn(),
                  thunk = loadComments();

            fetch.mockResolvedValue({
                ok: false,
            })

            await thunk(dispatch, () => ({}));
            const {calls} = dispatch.mock;
            const [start, end] = calls;

            expect(start[0].type).toBe(loadComments.pending().type);
            expect(end[0].type).toBe(loadComments.rejected().type);
            expect(end[0].payload).toBe('Помилка');
        });

        it('loadPosts', async () => {
            const dispatch = jest.fn(),
                  thunk = loadPosts();

            fetch.mockResolvedValue({
                ok: false,
            })

            await thunk(dispatch, () => ({}));
            const {calls} = dispatch.mock;
            const [start, end] = calls;

            expect(start[0].type).toBe('my/posts/pending');
            expect(end[0].type).toBe(loadPosts.rejected().type);
            expect(end[0].meta.rejectedWithValue).toBe(true);
        });

        it('loadUsers', async () => {
            const dispatch = jest.fn(),
                  thunk = loadUsers();

            fetch.mockResolvedValue({
                ok: false,
            })

            await thunk(dispatch, () => ({}));
            const {calls} = dispatch.mock;
            const [start, end] = calls;

            expect(start[0].type).toBe(loadUsers.pending().type);
            expect(end[0].type).toBe('my/users/rejected');
        });
    })
})