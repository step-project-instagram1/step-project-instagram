

export default function Button({btnText, className, clickHandler, type}){

    return(
        <button className={className} style={{'cursor': 'pointer'}}
                type={type}
                onClick={clickHandler}
        >{btnText}</button>
    )
}