import Button from "../Button";
import './style.scss'
export default function Form({className, onSubmit, reference}){

    return (
        <form onSubmit={onSubmit} noValidate className='form'>
            <textarea ref={reference}
                name="text"
                placeholder='Додайте коментар...'
                className={className}
            />
            <div>
                <Button btnText='Опублікувати' type="submit"/>
            </div>
        </form>
    )
}