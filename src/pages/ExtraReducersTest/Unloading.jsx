import './style.scss'

const Unloading = ({obj}) => {
    let arr = [];
    for (let key in obj) {
        arr.push({key: `${key}`, value: `${obj[key]}`})
    }
    return (
        <div className='unloading'>
            <ul className='unloading_ul'>
                {
                    arr.map(_ => (
                        <li key={Math.random()} className="unloading_ul_li"><span>{_.key}:</span><span>{_.value}</span>
                        </li>
                    ))
                }
            </ul>
        </div>
    )
}
export default Unloading