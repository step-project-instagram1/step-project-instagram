import Button from "../../components/Button";
import ReactDOM from "react-dom/client";
import DotsIcon from "../../components/Icons/Dots";
import './style.scss'
import PhotoPost from "../../components/PhotoPost";
import Modal from "../../components/Modal";
import {useSelector, useDispatch} from "react-redux";
import {loadComments, loadPost, loadPosts, loadUser, loadUsers,} from "../../storage/asyncThunks"
import {setFriends, setIsFriend, setIsOpen, setLikes} from "../../storage/slice"
import {useEffect, useState} from "react";
import Loader from "../../components/Loader";
import {useParams} from "react-router-dom";


export default function Profile ({likePhoto, addFriend}){
    const dispatch = useDispatch()
    const {isOpen, user, posts, post, like, isFriend, users,comments,account,userStatus, postStatus, error} = useSelector(state => ({
        isOpen:state.my.isOpen,
        user: state.my.user.arr,
        error: state.my.user.err,
        friends: state.my.friends,
        posts: state.my.posts.arr,
        post: state.my.post.arr,
        userStatus: state.my.user.status,
        postStatus: state.my.post.status,
        account: state.my.activeAccount,
        like: state.my.like,
        isFriend:state.my.isFriend,
        users: state.my.users.arr,
        comments: state.my.comments.arr
    }))
    useEffect(() => {dispatch(loadUsers())}, [dispatch]);
    useEffect(() => {dispatch(loadPosts()) },[dispatch]);
    useEffect(() => {dispatch(loadComments()) },[dispatch]);
    useEffect(() => {dispatch(setIsOpen(false))}, [true]);
    useEffect(()=>{
        dispatch(setLikes(posts.filter(el=>el.likes.includes(account.id) ).map(e => e._id)))
    }, [posts])
    useEffect(()=>{
        dispatch(setFriends(users.filter(el=>el._id === account.id)[0]?.friends || []))
    }, [users])
    useEffect(()=>{
        dispatch(setIsFriend(users.filter(el=>el._id === account.id)[0]?.friends || []))
    }, [users])

    function openModal (e, id, likes){
        dispatch(setIsOpen(true))
        dispatch(loadPost(id.postId))
    }
    function hideModal(e){
        e.stopPropagation();
        dispatch(setIsOpen(false))
    }
    const param = useParams()
    useEffect(()=>{
        param.id && dispatch(loadUser(param.id))
    }, [param.id])

    if (error){
        return <p>Something get wrong</p>
    } else {
        if (userStatus === 'pending'){
            return <Loader/>
        } else {
            return (
                <main className='profile-main'>
                    {<header className='profile-header'>
                        <div className='profile-header__icon'>
                            <img src={user.photo} alt="userPhoto"/>
                        </div>
                        <div className='profile-header__info'>
                            <div className='profile-header__info__nickname'>
                                <h2>{user.nickName}</h2>
                                {user._id !== account.id && <Button
                                    btnText={!isFriend.includes(user._id) ? 'Підписатися' : 'Відстежується'}
                                            className='profile-header__btn'
                                            clickHandler={(e) => addFriend(e, user._id)}
                                    />}
                                <Button btnText="Повідомлення" className='profile-header__btn'/>
                                <DotsIcon/>
                            </div>
                            <div className='profile-header__info__detail'>
                                <p><span>37</span> дописів</p>
                                <p><span>148</span> читачів</p>
                                <p><span>158</span> стежать</p>
                            </div>
                            <p className='profile-header__info__name'>{user.fullName}</p>
                        </div>
                    </header>}
                    <section className="profile-section">
                        <div className="profile-section__head">
                            <div className="profile-section__head__icon">
                                <svg aria-label="" className="_ab6-" color="#262626" fill="#262626" height="12"
                                     role="img"
                                     viewBox="0 0 24 24" width="12">
                                    <rect fill="none" height="18" stroke="currentColor" strokeLinecap="round"
                                          strokeLinejoin="round" strokeWidth="2" width="18" x="3" y="3"></rect>
                                    <line fill="none" stroke="currentColor" strokeLinecap="round" strokeLinejoin="round"
                                          strokeWidth="2" x1="9.015" x2="9.015" y1="3" y2="21"></line>
                                    <line fill="none" stroke="currentColor" strokeLinecap="round" strokeLinejoin="round"
                                          strokeWidth="2" x1="14.985" x2="14.985" y1="3" y2="21"></line>
                                    <line fill="none" stroke="currentColor" strokeLinecap="round" strokeLinejoin="round"
                                          strokeWidth="2" x1="21" x2="3" y1="9.015" y2="9.015"></line>
                                    <line fill="none" stroke="currentColor" strokeLinecap="round" strokeLinejoin="round"
                                          strokeWidth="2" x1="21" x2="3" y1="14.985" y2="14.985"></line>
                                </svg>
                                <span>Дописи</span>
                            </div>
                            <div className="profile-section__head__icon">
                                <svg aria-label="" className="_ab6-" color="#8e8e8e" fill="#8e8e8e" height="12"
                                     role="img"
                                     viewBox="0 0 24 24" width="12">
                                    <path
                                        d="M10.201 3.797 12 1.997l1.799 1.8a1.59 1.59 0 0 0 1.124.465h5.259A1.818 1.818 0 0 1 22 6.08v14.104a1.818 1.818 0 0 1-1.818 1.818H3.818A1.818 1.818 0 0 1 2 20.184V6.08a1.818 1.818 0 0 1 1.818-1.818h5.26a1.59 1.59 0 0 0 1.123-.465Z"
                                        fill="none" stroke="currentColor" strokeLinecap="round" strokeLinejoin="round"
                                        strokeWidth="2"></path>
                                    <path
                                        d="M18.598 22.002V21.4a3.949 3.949 0 0 0-3.948-3.949H9.495A3.949 3.949 0 0 0 5.546 21.4v.603"
                                        fill="none" stroke="currentColor" strokeLinecap="round" strokeLinejoin="round"
                                        strokeWidth="2"></path>
                                    <circle cx="12.072" cy="11.075" fill="none" r="3.556" stroke="currentColor"
                                            strokeLinecap="round" strokeLinejoin="round" strokeWidth="2"></circle>
                                </svg>
                                <span>Позначено</span>
                            </div>
                        </div>
                        <div className="profile-section__posts">
                            {posts.map((item) => {
                                if (item.userId === user._id) {
                                    return (<PhotoPost
                                        clickHandler={openModal}
                                        photoPost={item.photo}
                                        likes={!item.likes.includes(account.id) ? (like.includes(item._id) ? item.likes.length+1 : item.likes.length)
                                            : (like.includes(item._id) ? item.likes.length : item.likes.length-1)}
                                        key={item._id}
                                        postId={item._id}
                                        likePhoto={likePhoto}
                                        commentsLength={comments.filter(e => e.postId === item._id).length}
                                    />)
                                }
                            })}
                        </div>
                    </section>
                    {isOpen && <Modal hideModal={hideModal}
                                       postImg={post.photo}
                                       likes={post.likes}
                                       id={post._id}
                                       isLike={!!like.includes(post._id)}
                                       userId={post.userId}
                                       nickName={user.nickName}
                                       userPhoto={user.photo}
                                       likePhoto={likePhoto}
                                       text={post.text}
                                       postId={post._id}
                                       comments={comments.filter(e => e.postId === post._id)}
                                       user={users}
                                       status={postStatus}
                                      userClick={()=> {dispatch(setIsOpen(false))}}
                    />}
                </main>
            )
        }}
}