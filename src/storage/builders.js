const myBuilder = (builder, extraReducer, atState) => {
    builder
        .addCase(extraReducer.pending, state => {
            state[atState].status = 'pending';
        })
        .addCase(extraReducer.fulfilled, (state, action) => {
            state[atState].arr = action.payload;
            state[atState].status = 'fullfiled';
            state[atState].err = '';
        })
        .addCase(extraReducer.rejected, (state, action) => {
            state[atState].err = action.payload;
            state[atState].status = 'rejected';
        })
}

export default myBuilder;