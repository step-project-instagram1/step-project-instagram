import{configureStore,  combineReducers} from "@reduxjs/toolkit";
import {persistReducer} from "redux-persist";
import storage from 'redux-persist/lib/storage';

import reducer from './slice'; // slice.reducer

const rootReducer = combineReducers({
    my: reducer,
});

const persistConfig = {
    key: 'root',
    storage,
}

const persistedReducer = persistReducer(persistConfig, rootReducer);

const store = configureStore({
    reducer: persistedReducer,
    middleware: (getDefaultMiddleware) => getDefaultMiddleware({serializableCheck: false}),
});

export const setupStore = preloadedState => { //Тільки для обгортання тесту fetch
    return configureStore({
        reducer:  rootReducer,
        preloadedState,
    })
}

export default store;